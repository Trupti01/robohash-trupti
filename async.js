// 3. Using Async

var fs = require('fs');
const superagent = require('superagent');
var data = ['data1','data2','data3'];
var res =[];
for(i=0;i<data.length;i++){
data[i]=Math.random().toString(36).substring(2, 7);
}
function RandomString(data) {
    console.log(`Random string is :${data}`);
}


function writeFilePromise(filelocation, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(filelocation, data, (err) => {
            if (err) {
                reject("not able to write content in file");
            }
            resolve(data)
        });
    });
}


async function robohash(){
    try{
        for(i=0;i<data.length;i++){
            RandomString(data[i])
        res[i] = await superagent.get(`https://robohash.org/${data[i]}`);
        const all = await Promise.all(res) 
        const img = all.map((i=>i.request.url))
        console.log(img);
        writeFilePromise("./robot_image.txt", img.join("\n"))
        console.log("image saved successfully!")
        }
    } catch (err) {
        console.log(err);
    }
}
robohash()