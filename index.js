var fs = require("fs");
const superagent = require("superagent");

// 3.Using Async Await

//Defining the function
function readFilePromise(fileLocation) {
    return new Promise((resolve, reject) => {
        fs.readFile(fileLocation, (err,randomText) => {
            if(err){
                reject("Not able to write content inside the File");
            }
            resolve(randomText);
        });
    });
};

function writeFilePromise(fileLocation,result) {
    return new Promise((resolve, reject) => {
        fs.writeFile(fileLocation, result, (err) => {
            if(err){
                reject("Not able to write content inside the file");
            }
            resolve();
        });
    });
};

//Calling the function
async function getRoboImage() {
         try {
           const randomText = await readFilePromise('./GetRandomString.txt');
           console.log(`Random string  is ${randomText}`)
           const res1 = superagent.get(`https://robohash.org/${randomText}`);
           const res2 = superagent.get(`https://robohash.org/${randomText}`);
           const res3 = superagent.get(`https://robohash.org/${randomText}`);
    
           const all = await Promise.all([res1, res2, res3]) ;
           const images= all.map((el) => el.request.url);
           console.log(images);
           await writeFilePromise('./robot_image.txt', images.join("\n"));
           console.log('sucessfully written the file');
         } catch (err) {
             console.log(err);
             }       
         }
         getRoboImage();

// 2.Using Promise

/*[  function readFilePromise(fileLocation) {
    return new Promise((resolve, reject) => {
        fs.readFile(fileLocation, (err,randomText) => {
            if(err){
                reject("Not able to write content inside the File");
            }
            resolve(randomText);
        });
    });
};

function writeFilePromise(fileLocation,result) {
    return new Promise((resolve, reject) => {
        fs.writeFile(fileLocation, result, (err) => {
            if(err){
                reject("Not able to write content inside the file");
            }
            resolve();
        });
    });
};

readFilePromise('./GetRandomString.txt')
   .then((randomText) => {
    console.log(`my string  is ${randomText}`)
    return superagent.get(`https://robohash.org/${randomText}`)
   })
    .then((res) => {
     console.log('random string image is ', res.request.url)
     return writeFilePromise('./roboimage.txt', res.request.url)
   })
   .then(() => {
     console.log('sucessfully written the file')
   })
   .catch((err) => {
     console.log(err)
   }) ]*/


 
// 1. Using Callbacks

/*[ fs.readFile("GetRandomString.txt","utf8",(err,data) => {
    if(err){
        console.log("error in reading the file",err);
        return;
    }

    //Display the file content
    console.log(`the random string of robo is ${data}`);

   superagent
    .get(`https://robohash.org/${data}`)
    .end((err, res) => {
        if(err){
            console.log("not able to retrive correct robo image",err);
            return;
        }

        // Calling the end function will send the request
        console.log(res.request.url);

fs.writeFile("./robotImage.txt",res.request.url,(err) =>{
    if(err){
        console.log("not able to write robo image",err);
        return;
    }
    console.log("Robo image succsesfully found");
        });
    });
}); ]*/
  